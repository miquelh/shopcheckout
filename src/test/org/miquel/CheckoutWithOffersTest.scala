package org.miquel

import org.scalatest.{FlatSpec, Matchers}

/**
  * EileanTech Archipelago Framework
  *
  * Copyright EileanTech Ltd. (2015)
  */
class CheckoutWithOffersTest extends FlatSpec with Matchers {

  import CheckoutWithOffers._

   val testPricing = Map[String, Int]("Apple" -> 60, "Orange" -> 25)
   val checkoutSystemNoOffers = CheckoutWithOffers(testPricing)
   val checkoutSystemWithOffers = CheckoutWithOffers(testPricing, Map("Apple" -> buyOneGetOneFree, "Orange" -> threeForTwo))


   "Checkout with no offers" should "price a basket" in {
     checkoutSystemNoOffers.priceBasket(Seq("Apple", "Apple", "Orange", "Apple")) shouldBe (205, Seq())
     checkoutSystemNoOffers.priceBasket(Seq("Apple", "Orange", "Orange", "Apple")) shouldBe (170, Seq())
   }

   it should "price an empty basket" in {
     checkoutSystemNoOffers.priceBasket(Seq()) shouldBe (0, Seq())
   }

   it should "price a basket and return unpriced items along the price" in {
     checkoutSystemNoOffers.priceBasket(Seq("Apple", "Orange", "Chocolate", "CocaCola")) shouldBe (85, Seq("Chocolate", "CocaCola"))
   }
  
  "Checkout with offers" should "price a basket" in {
    checkoutSystemWithOffers.priceBasket(Seq("Apple", "Apple", "Orange", "Apple")) shouldBe (145, Seq())
    checkoutSystemWithOffers.priceBasket(Seq("Apple", "Apple", "Orange", "Apple", "Apple")) shouldBe (145, Seq())
    checkoutSystemWithOffers.priceBasket(Seq("Apple", "Orange", "Orange", "Apple")) shouldBe (110, Seq())
    checkoutSystemWithOffers.priceBasket(Seq("Apple", "Orange", "Orange", "Orange", "Apple")) shouldBe (110, Seq())
  }

  it should "give a zero price and return all items as unpriced for a totally wrong basket" in {
    val oddBasket = Seq("Still Water", "Peanuts", "Chocolate", "CocaCola")
    checkoutSystemWithOffers.priceBasket(oddBasket) shouldBe (0, oddBasket)
  }

  it should "price an empty basket" in {
    checkoutSystemWithOffers.priceBasket(Seq()) shouldBe (0, Seq())
  }

  it should "price a basket and return unpriced items along the price" in {
    val basket = Seq("Apple", "Apple", "Orange", "Orange", "Orange", "Chocolate", "CocaCola")
    checkoutSystemWithOffers.priceBasket(basket) shouldBe (110, Seq("Chocolate", "CocaCola"))
  }

  it should "price a single item basket" in {
    checkoutSystemWithOffers.priceBasket(Seq("Apple")) shouldBe (60, Seq())
    checkoutSystemWithOffers.priceBasket(Seq("Orange")) shouldBe (25, Seq())
  }
}
