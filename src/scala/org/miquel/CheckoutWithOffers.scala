package org.miquel

/**
 * EileanTech Archipelago Framework
 *
 * Copyright EileanTech Ltd. (2015)
 */


/**
 * Notes for Step2: After thinking to adapt the simple Step1 solution to the offers requirement I thought that a better
 * approach to calculate prices would be to group the basket by product and a pricing function will be applied to each.
 * The end result will be the sum of the partial totals
 * As an additional improvement to previous version the method priceBasket will use partition to separate those items
 * with or without prices
 *
 * A few predefined offer functions are provided in the companion object
 *
 * Provides checkout functionality for baskets(lists of items) against a pricing table (represented as a map)
 * Special offer pricing functions can be injected externally as a map of produtc to pricing function.
 * Pricing functions will be of the form
 * Map[item:String, (itemPrice: Int, itemCount: Int) => result:Int]
 *
 *
 * @param pricing The pricing table, a map from String to prices (in pences)
 */
case class CheckoutWithOffers(pricing: Map[String, Int], offersFn: Map[String,(Int,Int) => Int] = Map()) {
  import CheckoutWithOffers._

   /**
    * Prices a basket. This method returns the price of a given basket next to the items for which no price was found
    *
    * @param basket the items to be priced
    *
    * @return A tuple with the price in pences and a seq of unpriced items
    */
   def priceBasket(basket: Seq[String]): (Int, Seq[String]) = {
     val (priced,unpriced) = basket.partition(pricing.contains)
     val total = priced.groupBy(identity).map {case (item, group) =>
       val count = group.size
       val itemPrice = pricing(item)
       offersFn.get(item).map(f => f(itemPrice, count)).getOrElse(normalPricing(itemPrice, count))
     }.sum

     (total, unpriced)
   }

 }


object CheckoutWithOffers {
  val normalPricing = (itemPrice: Int, itemCount: Int) => itemCount * itemPrice
  val buyOneGetOneFree = (itemPrice: Int, itemCount: Int) => (itemCount / 2 + itemCount % 2) * itemPrice
  val threeForTwo = (itemPrice: Int, itemCount: Int) => ((itemCount / 3) * 2 + itemCount % 3) * itemPrice
}

