package org.miquel

import org.scalatest.{FlatSpec, Matchers}

/**
 * EileanTech Archipelago Framework
 *
 * Copyright EileanTech Ltd. (2015)
 */
class CheckoutTest extends FlatSpec with Matchers {

  val testPricing = Map[String, Int]("Apple" -> 60, "Orange" -> 25)
  val checkoutSystem = Checkout(testPricing)


  "Checkout" should "price a basket" in {
    checkoutSystem.priceBasket(Seq("Apple", "Apple", "Orange", "Apple")) shouldBe (205, Seq())
    checkoutSystem.priceBasket(Seq("Apple", "Orange", "Orange", "Apple")) shouldBe (170, Seq())
  }

  it should "price a single item basket" in {
    checkoutSystem.priceBasket(Seq("Apple")) shouldBe (60, Seq())
    checkoutSystem.priceBasket(Seq("Orange")) shouldBe (25, Seq())
  }

  it should "price an empty basket" in {
    checkoutSystem.priceBasket(Seq()) shouldBe (0, Seq())
  }

  it should "price a basket and return unpriced items along the price" in {
    checkoutSystem.priceBasket(Seq("Apple", "Orange", "Chocolate", "CocaCola")) shouldBe (85, Seq("Chocolate", "CocaCola"))
  }

  it should "give a zero price and return all items as unpriced for a totally wrong basket" in {
    val oddBasket = Seq("Still Water", "Peanuts", "Chocolate", "CocaCola")
    checkoutSystem.priceBasket(oddBasket) shouldBe (0, oddBasket)
  }

}
