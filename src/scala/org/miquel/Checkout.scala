package org.miquel

/**
 * EileanTech Archipelago Framework
 *
 * Copyright EileanTech Ltd. (2015)
 */


/**
 * Provides checkout functionality for baskets(lists of items) against a pricing table (represented as a map)
 * 
 * @param pricing The pricing table, a map from String to prices (in pences)
 */
case class Checkout(pricing: Map[String, Int]) {

  /**
   * Prices a basket. This method returns the price of a given basket next to the items for which no price was found
   * 
   * @param basket the items to be priced
   *              
   * @return A tuple with the price in pences and a seq of unpriced items
   */
  def priceBasket(basket: Seq[String]): (Int, Seq[String]) = {
    basket.foldLeft((0, Seq[String]())){
      case (tot, itm) if pricing.contains(itm) => (tot._1 + pricing(itm), tot._2)
      case (tot, itm) => (tot._1, tot._2 :+ itm)
    }
  }
}


